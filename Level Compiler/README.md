# Level Compiler
Internal tool to be used by team to create level binaries
If you're using Windows, you'll need to install win32api. This can be done by running the command `pip install win32api`.

## Spawning objects
Parameters should be separated by a space. Parameters marked with "`()`" are mandatory. Those marked with "`[]`" are optional.
- Asteroid: `A (Y position) (Speed)`
- Ez Enemy: `E (Y position) (Speed) [Power up] [Strafe at]`
- Med Enemy: `M (Y position) (Speed) [Power up] [Stop at] [Leave after # of frames] [Rate of fire]`
- Hard Enemy: `H (Y position) (Speed) [Power up] [Stop at] [Leave after # of frames] [Rate of fire]`

For the `[Power up]` parameter, you can use "normal" to indicate no power up being dropped. For `[Leave after # of frames]`, to have the enemy stay until it is defeated, simply insert `-1` as the parameter. For a parameter to be used, all parameters before it must be filled in.

## Level commands
- `PAUSE (# of frames)`: Waits that many frames until the next command is processed
- `WAIT`: Waits until all enemies have left the screen before continuing
- `END`: Indicates the level is over

## Running the program
```
python3 compile.py -i [path to input txt file] -o [path to output file]
```
