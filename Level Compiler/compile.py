#!/usr/bin/env python3

"""
compile.py

Copyright (c) 2022 Matthew Iasevoli & Team 5

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import pickle

import getopt, shutil, sys, os
if os.name == 'nt':
    import win32api, win32con

# Display output by default
output_on = True

def main(argv, cmd):
    # Default directory locations
    input_file = "level.txt"
    output_file = "level.level"
    
    # Check for errors in command arguments
    try:
        opts, args = getopt.getopt(argv,"chi:o:q",["help"])
    except getopt.GetoptError as e:
        print(e.args[0])
        usage()
        print("try '" + cmd + " --help' for more information")
        sys.exit(2)

    # Process command arguments
    for opt, arg in opts:
        if opt in ("-h" "--help"):
            # Display help
            help()
            sys.exit()
        elif opt == "-c":
            # Display copyright information
            copyright()
            sys.exit()
        elif opt == "-i":
            # Specify input file
            input_file = arg
        elif opt == "-o":
            # Specify output file
            output_file = arg
        elif opt == "-q":
            # Hide output (quiet mode)
            output_on = False

    # Read the input file
    try:
        with open(input_file) as f:
            input_lines = f.readlines()
    except Exception as e:
        console_output(e)
        console_output("Could not read the level input file")
        sys.exit(1)
    
    # Seperate the input into individual words
    input_words = []
    for line in input_lines:
        input_words.append(line.split())

    command_list = []
    keywords = {
        "END": (0, []),
        "WAIT": (1, []),
        "PAUSE": (2, [("num", True)]),
        "H": (252, [("num", True, "y"), ("num", True, "speed"), ("str", False, "weapon drop"), ("num", False, "stop at"), ("num", False, "leave after"), ("num", False, "fire speed")]),
        "M": (253, [("num", True, "y"), ("num", True, "speed"), ("str", False, "weapon drop"), ("num", False, "stop at"), ("num", False, "leave after"), ("num", False, "fire speed")]),
        "E": (254, [("num", True, "y"), ("num", True, "speed"), ("str", False, "weapon drop"), ("num", False, "strafe at")]),
        "A": (255, [("num", True, "y"), ("num", True, "speed")])
    }

    for words in input_words:
        keyword = words[0]
        arguments = words[1:]
        
        # Check if the keyword is valid
        if not keyword in keywords:
            print("Unrecognized keyword: " + keyword)
            sys.exit(1)

        # If it is, load data about the keyword
        keyword_code = keywords[keyword][0]
        max_argument_len = len(keywords[keyword][1])
        min_argument_len = 0
        for argument in keywords[keyword][1]:
            if argument[1]:
                min_argument_len += 1

        # Make sure arguments are valid in length
        if len(arguments) < min_argument_len or max_argument_len < len(arguments):
            console_output("Invalid arguments for " + keyword)
            sys.exit(1)

        # Process arguments
        processed_arguments = []
        try:
            for i in range(len(arguments)):
                if keywords[keyword][1][i][0] == "num":
                    processed_arguments.append(int(arguments[i]))
                else:
                    processed_arguments.append(arguments[i])
        except Exception as e:
            console_output(e)
            console_output("Invalid arguments for " + keyword)
            sys.exit(1)
        
        # If you reach here, the command has been processed successfully!
        new_tuple = (keyword_code, processed_arguments)
        command_list.append(new_tuple)

    console_output(command_list)

    try:
        with open(output_file, 'wb') as f:
            pickle.dump(command_list, f)
    except Exception as e:
        console_output(e)
        console_output("Could not save to file " + output_file)
        sys.exit(1)

def console_output(msg):
    '''Displays a message if the user is not in quiet mode'''
    if output_on:
        print(msg)

def usage():
    print("usage: compile.py [-i <input>] [-o <output>] [-chq]")

def help():
    usage()
    print("""
    c : display copyright information
    h : display this help page
    q : used quiet mode (no terminal output)

    i file : use `file' as the input
    o file : use `file' as the output

    For more info, see the README.md file included with this program.""")

def copyright():
    with open(__file__) as f:
        f_lines = f.readlines()
        for line in f_lines[3:24]:
            print(line.rstrip())

if __name__ == "__main__":
    main(sys.argv[1:], sys.argv[0])

