import arcade
import arcade.gui
import config
from menu import MainMenuView



class GameOverView(arcade.View):
    """ Class to manage the game-over screen """

    def __init__(self, score, level_file):
        from main import GameView
        super().__init__()
        self.window.set_mouse_visible(True)
        # Draw background and final score
        self.background = arcade.load_texture("assets/screens/background.jpg")
        self.final_score = score
        # GUI
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        # Render button
        default_style = {
            "font_name": "Kenney Rocket",
            "font_size": 30,
            "font_color": arcade.color.JAZZBERRY_JAM,
            "border_width": 50,
            "border_color": None,
            "bg_color": (15, 1, 56, 255),

            # Used if button is pressed
            "bg_color_pressed": arcade.color.CORN,
            "border_color_pressed": arcade.color.CORN,  # also used when hovered
            "font_color_pressed": arcade.color.WHITE,
        }
        # Align buttons
        self.v_box = arcade.gui.UIBoxLayout()
        # Create buttons
        resume_button = arcade.gui.UIFlatButton(text="Restart Level", width=650, height=50, style=default_style)
        main_menu_button = arcade.gui.UIFlatButton(text="Main Menu", width=650, height=50, style=default_style)
        quit_button = arcade.gui.UIFlatButton(text="Quit", width=650, height=50, style=default_style)
        # Add buttons
        self.v_box.add(resume_button)
        self.v_box.add(main_menu_button)
        self.v_box.add(quit_button)
        # Handle Clicks
        @resume_button.event("on_click")
        def on_click_start_button(event):
            # Resume Game
            game = GameView()
            game.setup()
            game.set_level(level_file)
            self.window.show_view(game)
        # Return to main menu
        @main_menu_button.event("on_click")
        def on_click_exit_button(event):
            self.window.show_view(MainMenuView())
        # Exit the game
        @quit_button.event("on_click")
        def on_click_exit_button(event):
            arcade.exit()

        # Create a widget to hold the v_box widget, that will center the buttons
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box)
        )

    def on_draw(self):
        """ Draw the game-over screen """
        self.clear()
        self.background.draw_sized(config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2,
                                   config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
        # Draw text
        arcade.draw_text("Game Over", config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 1.3,
                         arcade.color.YELLOW, font_size=70,
                         font_name="Kenney Rocket", anchor_x="center")
        # Draw final score
        score_text = f"Your Score: {self.final_score}"
        arcade.draw_text(score_text, config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 1.5,
                         arcade.color.WHITE, 30, font_name="Kenney Rocket", anchor_x="center")
        # Draw Buttons
        self.manager.draw()

