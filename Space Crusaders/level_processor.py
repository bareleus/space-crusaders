import arcade
import pickle

from enemy import *

def open_level(level_file_path):
    with open(level_file_path, 'rb') as level_file:
        unpickler = pickle.Unpickler(level_file)
        level_data = unpickler.load()
    return level_data   

class LevelProcessor:
    
    def __init__(self, level_data):
        self._level_data = level_data
        self._level_counter = 0
        self._pause_timer = 0
        self.level_cleared = False
    
    def update(self, enemy_count):
        # Load current instruction data
        instruction = self._level_data[self._level_counter][0]
        arguments = self._level_data[self._level_counter][1]
        
        # Spawn enemies simultaneously
        new_enemy_list = arcade.SpriteList()
        while instruction >= 128:
            if instruction == 255:
                new_enemy = Asteroid("assets/meteor.png", 2.5, hit_box_algorithm="Detailed")
            elif instruction == 254:
                new_enemy = EzEnemy("assets/ship_1.png", 1.5)            
            elif instruction == 253:
                new_enemy = MedEnemy("assets/ship_3.png", 1.5)
            else:
                new_enemy = HardEnemy("assets/ship_4.png", 1.5)
            new_enemy.spawn_from_args(arguments)
            new_enemy_list.append(new_enemy)
            self._level_counter += 1
            instruction = self._level_data[self._level_counter][0]
            arguments = self._level_data[self._level_counter][1]
        
        # Process one instruction and then return to the game
        if instruction == 0:
            self._end(enemy_count)
        elif instruction == 1:
            self._wait(enemy_count, len(new_enemy_list))
        elif instruction == 2:
            self._pause(arguments[0])
        
        return new_enemy_list
    
    def _end(self, enemy_count):
        self.level_cleared = enemy_count <= 0
    
    def _wait(self, enemy_count, new_enemy_count):
        if enemy_count == 0 and new_enemy_count == 0:
            self._level_counter += 1
    
    def _pause(self, frames):
        self._pause_timer += 1
        if self._pause_timer > frames:
            self._level_counter += 1
            self._pause_timer = 0