import arcade
import config
import main
import random
from menu import MainMenuView


class LevelStage(arcade.View):
    def __init__(self, score, lives, level_file, level):
        super().__init__()
        # Draw background
        self.background = arcade.load_texture("assets/screens/background.jpg")
        # Calculate bonus and final score
        self.final_score = score
        self.current_lives = lives
        self.bonus = self.current_lives * 100
        self.total_score = self.final_score + self.bonus
        self.level_file = level_file
        self.new_level = config.BASE_LEVELS.get(self.level_file, (None, None))[1]
        self.password = config.BASE_LEVELS.get(self.new_level, (None, None))[0]
        self.level = level + 1 if level != 0 else 0
        # Play sound
        self.level_sound = arcade.play_sound(arcade.load_sound("sounds/level.wav"))

    def on_draw(self):
        self.clear()
        self.background.draw_sized(config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2,
                                   config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
        # Draw Level Complete
        arcade.draw_text("Level Complete!", config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 1.3,
                         arcade.color.YELLOW, font_size=40,
                         font_name="Kenney Rocket", anchor_x="center")
        # Draw final score
        score_text = f"Your Score: {self.final_score}"
        arcade.draw_text(score_text, config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 1.5,
                         arcade.color.WHITE, 30, font_name="Kenney Rocket", anchor_x="center")

        # Draw bonus
        score_text = f"Bonus Score: {self.bonus}"
        arcade.draw_text(score_text, config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 1.7,
                         arcade.color.WHITE, 30, font_name="Kenney Rocket", anchor_x="center")

        # Draw total score
        score_text = f"Total Score: {self.total_score}"
        arcade.draw_text(score_text, config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2,
                         arcade.color.WHITE, 30, font_name="Kenney Rocket", anchor_x="center")
        
        # Draw password
        if self.password != None:
            password_text = f"Password: {self.password}"
            arcade.draw_text(password_text, config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 3,
                             arcade.color.BANANA_YELLOW, 30, font_name="Kenney Rocket", anchor_x="center")

        # Flashing the text
        if random.random() < 0.85:
            arcade.draw_text("Press ENTER to continue...", config.SCREEN_WIDTH / 2, 22,
                             arcade.color.AQUAMARINE, font_size=25,
                             font_name="Kenney High", anchor_x="center", bold=True)

    def on_key_press(self, symbol, modifier):
        if symbol == arcade.key.ENTER:
            arcade.stop_sound(self.level_sound)
            if self.new_level == None:
                #Back to main menu
                self.window.show_view(MainMenuView())
            else:
                # Load game view from main
                game = main.GameView()
                game.setup()
                game.score = self.total_score # update score
                game.level = self.level # update level
                # Move into the next level
                game.set_level(self.new_level)
                self.window.show_view(game)
