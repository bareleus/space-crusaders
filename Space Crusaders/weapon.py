import config
import arcade


class Weapon(arcade.Sprite):
    # Currently implemented types are normal, laser, double, drill
    weapon_type = "normal"
    
    def update(self):
        # Check if the weapon flies off-screen
        if self.right < 0:
            self.remove_from_sprite_lists()
        else: # Otherwise just move left
            self.center_x -= 3