import random
import arcade
import config


class InstructionView(arcade.View):
    def __init__(self, bg_music):
        """ Display instructions """
        super().__init__()
        # Import images
        self.background = arcade.load_texture("assets/screens/background.jpg")
        self.arrow_keys = arcade.load_texture("assets/screens/instruction/arrow_keys.png")
        self.space_bar = arcade.load_texture("assets/screens/instruction/space_bar.png")
        self.asteroid = arcade.load_texture("assets/screens/instruction/asteroid.png")
        self.enemy = arcade.load_texture("assets/screens/instruction/enemy.png")
        self.bg_music = bg_music

    def on_draw(self):
        """ Draw the instruction """
        self.clear()
        # Background
        self.background.draw_sized(config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2,
                                   config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
        # Draw images
        arcade.draw_lrwh_rectangle_textured(config.SCREEN_WIDTH - 350, config.SCREEN_HEIGHT - 185,
                                            120, 92, self.arrow_keys)
        arcade.draw_lrwh_rectangle_textured(config.SCREEN_WIDTH - 350, config.SCREEN_HEIGHT - 255,
                                            115, 38, self.space_bar)
        arcade.draw_lrwh_rectangle_textured(config.SCREEN_WIDTH - 370, config.SCREEN_HEIGHT - 350,
                                            70, 70, self.asteroid)
        arcade.draw_lrwh_rectangle_textured(config.SCREEN_WIDTH - 270, config.SCREEN_HEIGHT - 350,
                                            70, 70, self.enemy)
        # Draw texts
        arcade.draw_text("Instructions", 40, config.SCREEN_HEIGHT - 115,
                         arcade.color.AQUAMARINE, font_size=50, bold=True,
                         font_name="Kenney Pixel")

        arcade.draw_text("- Use the arrow keys to control your ship.", 80, config.SCREEN_HEIGHT - 170,
                         arcade.color.BANANA_YELLOW, font_size=20,
                         font_name="Kenney High")

        arcade.draw_text("- Press space to shoot.", 80, config.SCREEN_HEIGHT - 245,
                         arcade.color.BANANA_YELLOW, font_size=20,
                         font_name="Kenney High")

        arcade.draw_text("- Avoid colliding with obstacles.", 80, config.SCREEN_HEIGHT - 320,
                         arcade.color.BANANA_YELLOW, font_size=20,
                         font_name="Kenney High")
        # Flashing the text
        if random.random() < 0.85:
            arcade.draw_text(" Press ENTER to start", config.SCREEN_WIDTH / 2, 22,
                             arcade.color.AQUAMARINE, font_size=25,
                             font_name="Kenney High", anchor_x="center", bold=True)

    def on_key_press(self, symbol, modifier):
        """ Switch view after Enter is pressed """
        from main import GameView
        from menu import MainMenuView
        # Switch to game screen
        if symbol == arcade.key.ENTER:
            game_view = GameView()
            game_view.setup()
            game_view.level = 1
            self.window.show_view(game_view)
            self.bg_music = arcade.stop_sound(self.bg_music)
        # Go back to the main menu
        elif symbol == arcade.key.ESCAPE:
            self.window.show_view(MainMenuView())
            self.bg_music = arcade.stop_sound(self.bg_music)

