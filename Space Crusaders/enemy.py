import config
import arcade
from math import atan2, degrees, sin, cos

HEALTH_BAR_HEIGHT = 4

class Enemy(arcade.Sprite):
    weapon_drop = "normal"
    point_value = 0
    max_health = 0
    _frame_count = 0
    _stay_on_screen = True
    _timed = False
    
    def __init__(self, *args, **kwargs):
        super(Enemy, self).__init__(*args, **kwargs)
        self.current_health = self.max_health
        self.speed = 0
        self.strafe = 0
        self.weapon_drop = None
        self.left = config.SCREEN_WIDTH
    
    def spawn_from_args(self, arguments):
        """Have the enemy take on properties given by the level file"""
        pass
    
    def spawn():
        """Spawn an enemy using parameters instead of a list of arguments"""
        pass
    
    def update(self):
        # Update frame counter
        if self._timed: self._frame_count += 1
        
        # Move the enemy
        self.center_x -= self.speed
        self.center_y += self.strafe
        
        # Keep the enemy on screen
        if self._stay_on_screen:
            if self.top > config.PLAY_AREA:
                self.top = config.PLAY_AREA
                self.strafe = -abs(self.strafe)
            if self.bottom < 0:
                self.bottom = 0
                self.strafe = abs(self.strafe)
        elif self.bottom > config.SCREEN_HEIGHT or self.top < 0:
            self.remove_from_sprite_lists()
            
        # If the enemy goes past the player, despawn
        if self.right < 0: self.remove_from_sprite_lists()
    
    def draw_health_bar(self):
        # Draw health bar when enemies got hit by the player
        if self.current_health < self.max_health:
            arcade.draw_rectangle_filled(center_x=self.center_x,
                                         center_y=self.center_y + 27,
                                         width=self._health_bar_width,
                                         height=HEALTH_BAR_HEIGHT,
                                         color=arcade.color.RED,)
        # Calculate width based on health
        health_width = self._health_bar_width * (self.current_health / self.max_health)
        # Draw health bar when enemies appear
        arcade.draw_rectangle_filled(center_x=self.center_x - 0.5 * (self._health_bar_width - health_width),
                                     center_y=self.center_y + 27,
                                     width=health_width,
                                     height=HEALTH_BAR_HEIGHT,
                                     color=arcade.color.GREEN,)
    
    def on_hit(self, damage):
        """Called when the enemy is hit, returns points scored from hit"""
        self.current_health -= abs(damage)
        if self.current_health < 0:
            self.remove_from_sprite_lists()
            return self.point_value
        return 0
 
class Asteroid(Enemy):
    
    str_id = "A"
    int_id = 255
    
    point_value = 1
    
    def spawn_from_args(self, arguments):
        y = arguments[0]
        speed = arguments[1]
        self.spawn(y, speed)
    
    def spawn(self, y, speed):
        self.center_y = y
        self.speed = speed
    
    def draw_health_bar(self):
        pass
    
    def on_hit(self, damage):
        if damage < 0: # Drill damage set to -1, only it can destroy asteroids
            self.remove_from_sprite_lists()
            return self.point_value
        return 0

class EzEnemy(Enemy):
    
    str_id = "E"
    int_id = 254
    
    _health_bar_width = 30
    max_health = 2
    point_value = 1
    
    def spawn_from_args(self, arguments):
        num_arguments = len(arguments)
        y = arguments[0]
        speed = arguments[1]
        weapon_drop = arguments[2] if num_arguments >= 3 else "normal"
        strafe_at = arguments[3] if num_arguments >= 4 else -1
        self.spawn(y, speed, weapon_drop, strafe_at)
    
    def spawn(self, y, speed, weapon_drop="normal", strafe_at=-1):
        self.center_y = y
        self.speed = speed
        self.weapon_drop = weapon_drop
        self._strafe_at = strafe_at
    
    def update(self):
        if self.right <= self._strafe_at and not self.speed == 0:
            self.strafe = self.speed
            self.speed = 0
        super(EzEnemy, self).update()

class MedEnemy(Enemy):
    
    str_id = "M"
    int_id = 253
    
    _health_bar_width = 40
    max_health = 4
    point_value = 2
    _stay_on_screen = True
    shoot_interval = 60
    shoot_counter = 0
    _bullet_sprite = "assets/bullet_2.png"
    
    def spawn_from_args(self, arguments):
        num_arguments = len(arguments)
        y = arguments[0]
        speed = arguments[1]
        weapon_drop = arguments[2] if num_arguments >= 3 else "normal"
        strafe_at = arguments[3] if num_arguments >= 4 else 760
        duration = arguments[4] if num_arguments >= 5 else -1
        shoot_interval = arguments[5] if num_arguments >= 6 else 60
        self.spawn(y, speed, weapon_drop, strafe_at, duration, shoot_interval)
    
    def spawn(self, y, speed, weapon_drop="normal", strafe_at=760, duration=-1, shoot_interval=60):
        self.center_y = y
        self.speed = speed
        self._original_speed = speed
        self.weapon_drop = weapon_drop
        self._strafe_at = strafe_at
        self._duration = duration
        self._timed = not duration == -1
        self.shoot_interval = shoot_interval
        self.shoot_counter = 0
        
    
    def update(self):
        self.shoot_counter += 1
        
        # Stop at specified x value
        if self.right <= self._strafe_at and not self.speed == 0:
            self.speed = 0
        
        # Leave the screen after specified number of frames
        if not self._duration == -1 and self._frame_count >= self._duration:
            self._stay_on_screen = False
            self._duration = -1
            self.strafe = self._original_speed if self.center_y >= config.PLAY_AREA / 2 else -self._original_speed
        
        super(MedEnemy, self).update()
    
    def attack(self, player_x, player_y):
        x_diff = player_x - self.center_x
        y_diff = player_y - self.center_y
        angle = atan2(y_diff, x_diff)
        self.angle = degrees(angle) - 180
        
        if self.shoot_counter >= self.shoot_interval:
            new_bullet = arcade.Sprite(self._bullet_sprite, 0.5)
            new_bullet.center_x = self.center_x - 25
            new_bullet.center_y = self.center_y
            # Set hit_box
            new_bullet.set_hit_box(((0, 0.1), (1, -0.1)))
            # Angle for bullet
            new_bullet.angle = degrees(angle)
            # Bullet speed
            new_bullet.change_x = cos(angle) * self._original_speed * 2
            new_bullet.change_y = sin(angle) * self._original_speed * 2
            
            self.shoot_counter = 0
            return new_bullet
        
        # Don't return anything if we didn't shoot this time
        return None

class HardEnemy(MedEnemy):
    
    str_id = "H"
    int_id = 252
    
    _health_bar_width = 50
    _bullet_sprite = "assets/bullet_4.png"
    point_value = 3
    _strafe_frame_counter = 40
    max_health = 6
    
    def update(self):
        # Move to get out of the way, then stop
        if not self.strafe == 0:
            self._strafe_frame_counter -= 1
        if self._strafe_frame_counter <= 0 and self._stay_on_screen:
            self.strafe = 0
            self._strafe_frame_counter = 40
        super(HardEnemy, self).update()

class Bullet(arcade.Sprite):
    damage = 1