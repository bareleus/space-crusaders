import math
import random
import config
import arcade
import arcade.gui

# Import sprites from other modules
from player import Player, PlayerBullet
from enemy import *
from level_processor import LevelProcessor, open_level
from weapon import Weapon
# Other Screen
from menu import MainMenuView
from game_over import GameOverView
from pause import PauseView
from time import sleep

SCREEN_FRAME_ADJUSTMENT = 1.0
SPRITE_SCALING_PLAYER = 1.5


class GameView(arcade.View):
    def __init__(self):
        super().__init__()
        self.window.set_mouse_visible(False)
        # Background
        self.background = arcade.load_texture("assets/backgrounds/background_space.png")
        self.death_image = arcade.load_texture("assets/explosion.png")
        # Variables
        self.score = 0
        self.lives = 3
        self.level = 0
        # HUD
        self.gui_camera = None
        # Initialize variables for sprites
        self.player_sprite = None
        self.bullet_list = None
        self.enemy_list = None
        self.enemy_bullet_list = None
        self.weapon_list = None
        # Initialize level
        self.level_processor = None
        # Load sounds
        self.background_sound = arcade.load_sound("sounds/background.wav")
        self.gun_sound = arcade.load_sound("sounds/shoot.wav")
        self.game_over_sound = arcade.load_sound("sounds/game_over.wav")
        self.hit_sound = arcade.load_sound("sounds/hit.wav")
        self.destroyed_sound = arcade.load_sound("sounds/enemy_explode.wav")
        self.weapon_pickup_sounds = {
            "double": arcade.load_sound("sounds/weapon pickups/double.wav"),
            "drill": arcade.load_sound("sounds/weapon pickups/drill.wav"),
            "laser": arcade.load_sound("sounds/weapon pickups/laser.wav")
        }
        self.player_hit_sound = arcade.load_sound("sounds/player_hit.wav")
        self.level_file = "levels/1.lvl"

    def setup(self):
        # Setup HUD
        self.gui_camera = arcade.Camera(config.SCREEN_WIDTH, config.SCREEN_HEIGHT)
        # Setup Player
        self.player_sprite = Player("assets/ship_2.png", SPRITE_SCALING_PLAYER, hit_box_algorithm="Detailed")
        self.player_sprite.center_x = 64
        self.player_sprite.center_y = config.SCREEN_HEIGHT / 2 + 1
        # Setup Bullets
        self.bullet_list = arcade.SpriteList()
        self.enemy_bullet_list = arcade.SpriteList()
        # Setup Asteroids and Enemies
        self.enemy_list = arcade.SpriteList()
        # Setup Weapon Pick Ups
        self.weapon_list = arcade.SpriteList()
        # Load level
        self.level_processor = LevelProcessor(open_level(self.level_file))
        # Play sounds
        self.play_background = arcade.play_sound(self.background_sound, 0.4, 0.0, True, 1.0)
    
    def set_level(self, level_file):
        """Use to set a new level"""
        self.level_file = level_file
        self.level_processor = LevelProcessor(open_level(level_file))
    
    def on_draw(self):
        #arcade.start_render()
        # Draw background
        self.clear()
        arcade.draw_lrwh_rectangle_textured(0, 0, config.SCREEN_WIDTH, config.SCREEN_HEIGHT, self.background)
        # Draw sprites and bullets
        self.enemy_list.draw()
        self.bullet_list.draw()
        self.enemy_bullet_list.draw()
        if self.player_sprite.i_frames % 2 == 1 and not self.lives <= 0:
            self.player_sprite.draw() # Alternate on i-frames
        elif self.lives <= 0:
            arcade.draw_texture_rectangle(center_x=self.player_sprite.center_x,
                                          center_y=self.player_sprite.center_y,
                                          width=70,
                                          height=70,
                                          texture=self.death_image)
        self.player_sprite.draw_meter()
        # Weapon
        self.weapon_list.draw()
        # Draw health bar for enemies
        for enemy in self.enemy_list:
            enemy.draw_health_bar()
        # Draw HUD
        self.draw_hud()
        #arcade.finish_render()
    
    def on_update(self, delta_time):
        SCREEN_FRAME_ADJUSTMENT = delta_time * config.TARGET_FPS
        
        # Check if the game is over...
        if self.lives <= 0: self.game_over()

        # Add enemies from the level file
        new_enemies = self.level_processor.update(len(self.enemy_list))
        for enemy in new_enemies:
            self.enemy_list.append(enemy)
        
        # Check if the level is complete
        if self.level_processor.level_cleared and len(self.enemy_bullet_list) <= 0:
            arcade.stop_sound(self.play_background)
            from level_stage import LevelStage
            self.window.show_view(LevelStage(self.score, self.lives, self.level_file, self.level))
        
        # Update sprites
        self.bullet_list.update()
        self.enemy_bullet_list.update()
        self.enemy_list.update()
        self.player_sprite.update()
        self.weapon_list.update()

        # Check and remove bullets for the player
        for bullet in self.bullet_list:
            # Create collisions
            hit_enemies = arcade.check_for_collision_with_list(bullet, self.enemy_list)
            if len(hit_enemies) > 0: # If the bullet hits...
                # Make the bullet disappear
                bullet.remove_from_sprite_lists()
            # Remove the bullet if it files off-screen
            if bullet.left > config.SCREEN_WIDTH:
                bullet.remove_from_sprite_lists()
            
            # Process hit enemies
            for enemy in hit_enemies:
                weapon_drop = enemy.weapon_drop
                points = enemy.on_hit(bullet.damage)
                if not points == 0:
                    # The enemy was destroyed 
                    arcade.play_sound(self.destroyed_sound)
                    if not weapon_drop == "normal":
                        self.spawn_weapon(enemy.center_x, enemy.center_y, enemy.weapon_drop)
                    self.score += points
                else:
                    # It stil lives...
                    arcade.play_sound(self.hit_sound)
        
        # Have enemies shoot
        for enemy in self.enemy_list:
            if enemy.int_id <= 253:
                new_bullet = enemy.attack(self.player_sprite.center_x, self.player_sprite.center_y)
                if not new_bullet == None: self.enemy_bullet_list.append(new_bullet)

                if enemy.str_id == "H": # Have hard enemies dodge
                    for bullet in self.bullet_list:
                        distance = arcade.get_distance_between_sprites(bullet, enemy)
                        if 100 > distance > 25 and enemy.strafe == 0:
                            # Move away from the bullet
                            if enemy.center_y < bullet.center_y:
                                enemy.strafe = -enemy._original_speed
                            else:
                                enemy.strafe = enemy._original_speed
        
        # Create collisions for player
        collide_enemy = arcade.check_for_collision_with_list(self.player_sprite, self.enemy_list)
        collide_enemy_bullet = arcade.check_for_collision_with_list(self.player_sprite, self.enemy_bullet_list)
        collide_weapon = arcade.check_for_collision_with_list(self.player_sprite, self.weapon_list)
        
        # Check if the player collides with enemies and asteroids
        if collide_enemy or collide_enemy_bullet:
            if self.player_sprite.i_frames == -1: self.player_hit() # Only when not in i-frames
        # Check if bullet flies of screen
        for bullet in self.enemy_bullet_list:
            if bullet.right < 0:
                bullet.remove_from_sprite_lists()
        # Check if player picks up a weapon
        for weapon in collide_weapon:
            arcade.play_sound(self.weapon_pickup_sounds[weapon.weapon_type])
            self.player_sprite.pickup_weapon(weapon)
            if weapon.weapon_type == "drill": # Put a time limit on the drill power up
                self.player_sprite.weapon_duration = 600
            weapon.remove_from_sprite_lists()

    def on_key_press(self, symbol, modifier):
        if symbol == arcade.key.UP:
            self.player_sprite.up_pressed = True
        elif symbol == arcade.key.DOWN:
            self.player_sprite.down_pressed = True
        elif symbol == arcade.key.LEFT:
            self.player_sprite.left_pressed = True
        elif symbol == arcade.key.RIGHT:
            self.player_sprite.right_pressed = True
        elif symbol == arcade.key.SPACE:
            self.shoot()
        elif symbol == arcade.key.ESCAPE:
            arcade.stop_sound(self.play_background)
            self.window.show_view(PauseView(self))

    def on_key_release(self, symbol, modifier):
        if symbol == arcade.key.UP:
            self.player_sprite.up_pressed = False
        elif symbol == arcade.key.DOWN:
            self.player_sprite.down_pressed = False
        elif symbol == arcade.key.LEFT:
            self.player_sprite.left_pressed = False
        elif symbol == arcade.key.RIGHT:
            self.player_sprite.right_pressed = False
        elif symbol == arcade.key.ESCAPE:
            self.play_background = arcade.play_sound(self.background_sound, 0.4, 0.0, True, 1.0)

    def shoot(self):
        """Bullet that shoots out from the player"""
        # Create sprite for bullet
        if self.player_sprite.weapon_type == "double":
            bullet = PlayerBullet("assets/bullet_3.png", 0.3)
            bullet.set_hit_box(((0, 0.1), (1, -0.1)))
            bullet.damage = 2
        elif self.player_sprite.weapon_type == "laser":
            bullet = PlayerBullet("assets/bullet.png", 0.4)
            bullet.set_hit_box(((0, 0.2), (2, -0.2)))
            bullet.damage = 1
        elif self.player_sprite.weapon_type == "drill":
            bullet = PlayerBullet("assets/drill.png", 0.3)
            bullet.set_hit_box(((0, 0.2), (2, -0.2)))
            bullet.damage = -3
        else:  # normal
            bullet = PlayerBullet("assets/bullet_1.png", 0.5)
            bullet.set_hit_box(((0, 0.1), (1, -0.1)))
            bullet.damage = 1
        # Set positions
        bullet.center_x = self.player_sprite.center_x + 20
        bullet.center_y = self.player_sprite.center_y
        # Set speed for bullet
        bullet.change_x = 10
        self.bullet_list.append(bullet)
        # Play sound
        arcade.play_sound(self.gun_sound)

    def spawn_weapon(self, x, y, weapon_type="laser"):
        """Called to add a new weapon pick up on screen"""
        if not (weapon_type == "normal" or weapon_type == None):
            weapon_file = "assets/" + weapon_type + "_powerup.png"
            weapon = Weapon(weapon_file, 1.0)
            weapon.weapon_type = weapon_type
            weapon.center_x = x
            weapon.center_y = y
            self.weapon_list.append(weapon)

    def player_hit(self):
        """Called whenever the player takes damage"""
        self.lives -= 1
        if self.lives > 0:
            # Respawn
            self.player_sprite.i_frames = 120
            arcade.play_sound(self.player_hit_sound)
        else:
            self.player_sprite.visible = False

    def draw_hud(self):
        """Draw the HUD over the screen, including score and current lives"""
        # Activate HUD camera
        self.gui_camera.use()
        
        # Draw score on the screen
        score_text = f"Score: {self.score}"
        arcade.draw_text(
            score_text,
            10,
            config.SCREEN_HEIGHT - 60 * 0.5,
            arcade.csscolor.WHITE,
            20,
            # font_name="Kenney Mini Square"
            font_name="Kenney Blocks"
        )
        
        # Draw current level on the screen
        level_text = f"Level: {self.level}"
        arcade.draw_text(
            level_text,
            10,
            config.SCREEN_HEIGHT - 60 * 0.5,
            arcade.csscolor.WHITE,
            20,
            # font_name="Kenney Mini Square"
            font_name="Kenney Blocks",
            width=config.SCREEN_WIDTH,
            align="center"
        )

        # Draw lives on the screen
        lives_text = f"Health: {self.lives}"
        arcade.draw_text(
            lives_text,
            10,
            config.SCREEN_HEIGHT - 60 * 0.5,
            arcade.csscolor.WHITE,
            20,
            # font_name="Kenney Mini Square"
            font_name="Kenney Blocks",
            width=config.SCREEN_WIDTH - 20,
            align="right"
        )
        
        # Draw current power up on the screen
        weapon_text = f"{self.player_sprite.weapon_type}"
        if weapon_text == "normal":
            color1 = arcade.csscolor.WHITE_SMOKE
        elif weapon_text == "double":
            color1 = arcade.csscolor.YELLOW_GREEN
        elif weapon_text == "drill":
            color1 = arcade.csscolor.GREY
        elif weapon_text == "laser":
            color1 = arcade.csscolor.RED
        arcade.draw_text(
            weapon_text,
            -10,
            config.SCREEN_HEIGHT - 525,
            color1,
            10,
            # font_name="Kenney Mini Square"
            font_name="Kenney Blocks",
            width=config.SCREEN_WIDTH,
            align="right",
        )
        
    def game_over(self):
        arcade.play_sound(self.game_over_sound)
        arcade.stop_sound(self.play_background)
        self.on_draw()
        sleep(1)
        self.window.show_view(GameOverView(self.score, self.level_file))


def debug(message):
    if config.DEBUG:
        print(message)


def main():
    game = arcade.Window(config.SCREEN_WIDTH, config.SCREEN_HEIGHT, config.SCREEN_TITLE, vsync=config.SCREEN_VSYNC)
    # Set game icon
    from pyglet.image import load as pyglet_load
    game.set_icon(pyglet_load("assets/ship_2.ico"))
    # Start with main menu
    menu = MainMenuView()
    game.show_view(menu)
    start_game()

def start_game():
    arcade.run()


if __name__ == "__main__":
    main()
