import arcade
import arcade.gui
import config
from instruction import InstructionView
from easygui import enterbox, fileopenbox, msgbox

BACKGROUND_IMAGE = arcade.load_texture("assets/screens/background.jpg")
DEFAULT_STYLE = {
    "font_name": "Kenney Rocket",
    "font_size": 30,
    "font_color": arcade.color.JAZZBERRY_JAM,
    "border_width": 50,
    "border_color": None,
    "bg_color": (15, 1, 56, 255),

    # Used if button is pressed
    "bg_color_pressed": arcade.color.CORN,
    "border_color_pressed": arcade.color.CORN,  # also used when hovered
    "font_color_pressed": arcade.color.WHITE,
}

class MenuView(arcade.View):
    def __init__(self):
        super().__init__()
        self.window.set_mouse_visible(True)
        # GUI
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        
        # Align buttons
        self.v_box = arcade.gui.UIBoxLayout()
        
        # Create a widget to hold the v_box widget, that will center the buttons
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box)
        )
    
    def on_draw(self):
        """ Draw the menu """
        self.clear()
        # Background
        BACKGROUND_IMAGE.draw_sized(config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2,
                                   config.SCREEN_WIDTH, config.SCREEN_HEIGHT)

        # Draw texts
        arcade.draw_text(config.SCREEN_TITLE, config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT - 125,
                         arcade.color.CORN, font_size=70, bold=True,
                         anchor_x="center", font_name="Kenney Mini Square")
        
        arcade.draw_text("Version " + config.VERSION, config.SCREEN_WIDTH / 2, 22,
                 arcade.color.AQUAMARINE, font_size=25,
                 font_name="Kenney High", anchor_x="center", bold=True)
        
        self.manager.draw()

class MainMenuView(MenuView):
    def __init__(self):
        super().__init__()
        
        # Main menu music
        self.mainmenu_sound = arcade.load_sound("sounds/mainmenu.wav")
        self.play_mainmenu = arcade.play_sound(
            self.mainmenu_sound, 0.4, 0.0, True, 1.0)
        
        # Create buttons
        start_button = arcade.gui.UIFlatButton(text="Start", width=450, height=50, style=DEFAULT_STYLE)
        level_button = arcade.gui.UIFlatButton(text="Levels", width=450, height=50, style=DEFAULT_STYLE)
        exit_button = arcade.gui.UIFlatButton(text="Exit Game", width=450, height=50, style=DEFAULT_STYLE)
        
        # Add buttons
        self.v_box.add(start_button)
        self.v_box.add(level_button)
        self.v_box.add(exit_button)

        """Handle Clicks"""
        # Start Game
        @start_button.event("on_click")
        def on_click_start_button(event):
            self.instruction_view = InstructionView(self.play_mainmenu)
            self.window.show_view(self.instruction_view)
        # Level Select
        @level_button.event("on_click")
        def on_click_level_button(event):
            self.window.show_view(LevelSelectView())
            self.play_mainmenu = arcade.stop_sound(self.play_mainmenu)
        # Exit Game
        @exit_button.event("on_click")
        def on_click_exit_button(event):
            arcade.exit()


class LevelSelectView(MenuView):
    def __init__(self):
        super().__init__()
        
        # Create buttons
        password_button = arcade.gui.UIFlatButton(text="Password", width=450, height=50, style=DEFAULT_STYLE)
        load_file_button = arcade.gui.UIFlatButton(text="Load File", width=450, height=50, style=DEFAULT_STYLE)
        back_button = arcade.gui.UIFlatButton(text="Back", width=450, height=50, style=DEFAULT_STYLE)
        
        # Add buttons
        self.v_box.add(password_button)
        self.v_box.add(load_file_button)
        self.v_box.add(back_button)
        
        """Handle Clicks"""
        # Enter level password
        @password_button.event("on_click")
        def on_click_password_button(event):
            password_entered = enterbox("Password")
            if not password_entered == None:
                # Check if user's password is the list of base level passwords
                level_selected = False
                for key in config.BASE_LEVELS.keys():
                    if config.BASE_LEVELS[key][0] == password_entered and not level_selected:
                        from main import GameView
                        game_view = GameView()
                        game_view.setup()
                        game_view.level = config.BASE_LEVELS[key][2]
                        game_view.set_level(key)
                        self.window.show_view(game_view)
                        level_selected = True
                # If we didn't load a level, password must be wrong
                if not level_selected: msgbox("Incorrect password...")
        # Load level from file
        @load_file_button.event("on_click")
        def on_click_load_file_button(event):
            file_to_open = fileopenbox(default="*.lvl", filetypes=["*", "*.lvl"])
            game_view = None
            if not file_to_open == None:
                try:
                    from main import GameView
                    game_view = GameView()
                    game_view.setup()
                    game_view.set_level(file_to_open)
                    self.window.show_view(game_view)
                except Exception as e:
                    msgbox("There was a problem with this file: " + str(e))
                    arcade.stop_sound(game_view.play_background)
        # Back to main menu
        @back_button.event("on_click")
        def on_click_back_button(event):
            self.window.show_view(MainMenuView())
