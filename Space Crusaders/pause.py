import arcade
import config


class PauseView(arcade.View):
    def __init__(self, game_view):
        super().__init__()
        self.game_view = game_view

    def on_show_view(self):
        arcade.set_background_color(arcade.color.ORANGE)

    def on_draw(self):
        self.clear()

        # Draw texts
        arcade.draw_text("PAUSED", config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2 + 50,
                         arcade.color.BLACK, font_size=50, anchor_x="center")

        # Show instruction to return or reset the game
        arcade.draw_text("Press Esc. to return",
                         config.SCREEN_WIDTH / 2,
                         config.SCREEN_HEIGHT / 2,
                         arcade.color.BLACK,
                         font_size=20,
                         anchor_x="center")
        arcade.draw_text("Press ENTER to reset",
                         config.SCREEN_WIDTH / 2,
                         config.SCREEN_HEIGHT / 2 - 30,
                         arcade.color.BLACK,
                         font_size=20,
                         anchor_x="center")

        arcade.draw_text("Press CTRL to Return to Main Menu",
                         config.SCREEN_WIDTH / 2,
                         config.SCREEN_HEIGHT / 2 - 60,
                         arcade.color.BLACK,
                         font_size=20,
                         anchor_x="center")

    def on_key_press(self, key, modifiers):
        # Switch to game screen (resume game)
        if key == arcade.key.ESCAPE:
            self.window.show_view(self.game_view)
        # Start over and restart the game
        elif key == arcade.key.ENTER:
            from main import GameView
            game = GameView()
            game.setup()
            game.set_level(self.game_view.level_file)
            self.window.show_view(game)
        elif key == arcade.key.LCTRL or arcade.key.RCTRL:
            from menu import MainMenuView
            self.window.show_view(MainMenuView())
