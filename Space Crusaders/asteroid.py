import config
import arcade


class Asteroid(arcade.Sprite):
    def update(self):
        # Make sure asteroids stay in the area
        if self.center_y > config.SCREEN_HEIGHT - 65:
            self.center_y = config.SCREEN_HEIGHT - 65
        elif self.center_y < 40:
            self.center_y = 40
        # Set speed for asteroids
        self.center_x -= 3
