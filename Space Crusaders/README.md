# Space Crusaders
This is the release version of Team 5's project for Dr. Despang's software engineering class. For instructions, please see `instruction_manual.pdf` in the release. For licensing, please see LICENSE and sources.txt.

This project is a "Shoot 'Em Up" style video game with a retro feel, in the vein of early arcade games such as Galaga. There's multiple levels to play and support for adding more levels from files on your computer. Enjoy!!

v 1.0
