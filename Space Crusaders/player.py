import config
import arcade
from weapon import Weapon


class Player(arcade.Sprite):
    up_pressed = False
    down_pressed = False
    left_pressed = False
    right_pressed = False
    weapon_type = "normal"
    weapon_counter = 0
    weapon_duration = -1
    i_frames = -1

    def update(self):
        # Set the sprite's position based on keyboard input
        self.center_x += (self.right_pressed - self.left_pressed) * config.SPEED_MULTIPLIER
        self.center_y += (self.up_pressed - self.down_pressed) * config.SPEED_MULTIPLIER

        # Make sure the sprite stays in the play area
        if self.center_y < 0.5 * self.height:
            self.center_y = 0.5 * self.height
        elif self.center_y > config.SCREEN_HEIGHT - 55:
            self.center_y = config.SCREEN_HEIGHT - 55
        if self.center_x < 0.5 * self.width:
            self.center_x = 0.5 * self.width
        elif self.center_x > 200:
            self.center_x = 200
        
        # Weapon count down
        if not self.weapon_duration == -1:
            self.weapon_counter += 1
            if self.weapon_counter >= self.weapon_duration:
                self.weapon_type = "normal"
                self.weapon_duration = -1
                self.weapon_counter = 0
        
        # i-Frames count down
        if not self.i_frames == -1:
            self.i_frames -= 1
            if self.i_frames <= 0: self.i_frames = -1
    
    def pickup_weapon(self, weapon_pickup):
        self.weapon_type = weapon_pickup.weapon_type
        # Reset the weapon counter when you pick up a new weapon
        self.weapon_duration = -1
        self.weapon_counter = 0
    
    def draw_meter(self):
        # Draw meter bar for player if they have a power up that requires it
        if not self.weapon_duration == -1:
            arcade.draw_rectangle_filled(center_x=self.center_x,
                                         center_y=self.center_y + 30,
                                         width=32,
                                         height=4,
                                         color=arcade.color.BLUE,)
            # Calculate width based on time remaining
            meter_width = 32 * ((self.weapon_duration - self.weapon_counter) / self.weapon_duration)
            # Draw meter
            arcade.draw_rectangle_filled(center_x=self.center_x - 0.5 * (32 - meter_width),
                                         center_y=self.center_y + 30,
                                         width=meter_width,
                                         height=4,
                                         color=arcade.color.GREEN,)

class PlayerBullet(arcade.Sprite):
    damage = 1