SCREEN_WIDTH = 960
SCREEN_HEIGHT = 540
PLAY_AREA = 540 - 32
SCREEN_TITLE = "Space Crusaders"
SCREEN_VSYNC = True
TARGET_FPS = 60
DEBUG = True
SPEED_MULTIPLIER = 5.0
VERSION = "1.0.1"

BASE_LEVELS = {
    "levels/1.lvl": (None, "levels/2.lvl", 1),
    "levels/2.lvl": ("SPRSZ", "levels/3.lvl", 2),
    "levels/3.lvl": ("EFJWS", "levels/4.lvl", 3),
    "levels/4.lvl": ("EWWAD", None, 4),
}